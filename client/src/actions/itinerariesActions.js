const getItineraries=(cityId)=> async (dispatch, getState)=>{
    console.log(cityId);
    
    const response=await fetch("http://localhost:5000/api/"+cityId)
    .then(resp=>resp.json());
    dispatch({type:'GET_ITINERARIES',payload: response})
 
}

export default getItineraries