import axios from 'axios'


export const postUser = user => async (dispatch, getState) =>{
    const {email, password} = user 
    dispatch({
        type: 'LOADING_USER'
    })

    try {
        const { data }  = await axios.post(`http://localhost:5000/api/login`,{ email, password})
        dispatch({type:'POST_USER',payload: data})
        
    } catch (error) {
        console.log('Error User', error)
    }
 
};
export const googleUser = token =>dispatch=>{
   
  axios.get(`http://localhost:5000/api/login`,{token})
  .then(({ data }) => {
    const User = { user: data.data, token };
    console.log('User loaded front', User)
    dispatch({
      type: 'POST_USER',
      payload: User
    });
  })
  .catch(err => console.log(err));
};
     
export const logOut = () => dispatch => {
   dispatch({
      type: 'LOGOUT_SUCCESS'
    });
  };
  export const googleSuccess = token => dispatch => {
    return dispatch({
      type: 'GOOGLE_SUCCESS',
      payload: token
    });
  };
  