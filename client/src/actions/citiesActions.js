
const getAllCities=()=> async (dispatch, getState)=>{
    console.log('ciudades');
    
    const response=await fetch("http://localhost:5000/api/cities/all")
    .then(resp=>resp.json());
    dispatch({type:'GET_ALL_CITIES',payload: response})
 
}
export const getCity=(idCity)=> async (dispatch, getState)=>{
    console.log('city');
    
    const response=await fetch("http://localhost:5000/api/city/"+idCity)
    .then(resp=>resp.json());
    dispatch({type:'GET_CITY',payload: response})
 
}

export default getAllCities

