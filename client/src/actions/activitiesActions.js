import axios from 'axios'

export const getActivities=(idItinerary)=> async (dispatch, getState)=>{
    console.log('activities', idItinerary);
    
    const response  = await axios.get(`http://localhost:5000/api/activities/${idItinerary}`)
    console.log(response)
    dispatch({type:'GET_ACTIVITIES',payload: response.data})
 
}



