import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import Landing2 from './components/Landing2'
import Account from './components/Account'
import Cities from './components/Cities'
import Login from './components/Login'
import Itineraries from './components/itineraries'
import LoadUser from './components/LoadUser'
import './App.css';
import { googleUser } from "./actions/loginActions";


class App extends Component {
  componentDidMount() {
    store.dispatch(googleUser());
  }
render(){
  return (<BrowserRouter>
    <div className="App">
      <Switch>
        <Route exact path="/" component={Landing2} />
        <Route path="/Account" component={Account} />
        <Route path="/Cities" component={Cities} />
        <Route path="/Itineraries/:idCity" component={Itineraries} />
        <Route path="/Login" component={Login} />
        <Route path="/LoadUser/:token" component={LoadUser}/>
      </Switch>
    </div>
  </BrowserRouter>
  );
}
}
export default App;
