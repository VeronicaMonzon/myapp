import React from 'react';
import Adapter from 'enzyme-adapter-react-16'
import logo from '../imagenes/MYtineraryLogo.png'
import { shallow, mount, render,configure } from 'enzyme';

import {Header} from '../components/LandingPage'

configure({adapter: new Adapter()})
describe('A suite', function() {
  it('should render without throwing an error', function() {
    expect(shallow(<Header />).contains(<header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />           
        </header>)).toBe(false);
  });
});
