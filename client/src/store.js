import { createStore, applyMiddleware} from 'redux';
import reduxThunk from 'redux-thunk';
import rootReducer from './reducers';
import {composeWithDevTools} from 'redux-devtools-extension'

const initialState={};

const store=createStore(rootReducer,initialState,composeWithDevTools(applyMiddleware(reduxThunk)));

export default store;
    