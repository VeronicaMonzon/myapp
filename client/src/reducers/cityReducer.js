const initialState = {
    cities: [],
    city: ""
}
export default function (state = initialState, action) {
    console.log(action);

    switch (action.type) {
        case 'GET_ALL_CITIES':
            return {
                ...state,
                cities: action.payload
            }
        case 'GET_CITY':
            return {
                ...state,
                city: action.payload
            }


        default:
            return state;
    }
};