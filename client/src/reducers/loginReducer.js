var jwt_decode=require('jwt-decode')
const initialState={
    user:{},
    isAuth: false,
    isLoading: false,
    token: window.localStorage.getItem('token')
}
export default function(state=initialState,action) {
    console.log(action);
    
    switch (action.type) {
        case 'LOADING_USER':
            return{
                ...state,
                isLoading: true
            }
        case 'POST_USER':
            localStorage.setItem('token', action.payload.token)
            // console.log('linea 18')
            // console.log(jwt_decode(action.payload.token))
            return{
                user: jwt_decode(action.payload.token),
                token: action.payload.token,
                isAuth: true,
                isLoading: false
            }
      
         case 'LOGOUT_SUCCESS':
            localStorage.removeItem("token");
            return {
              ...state,
              token: null,
              isAuth: false,
              isLoading: false
            }
         case 'GOOGLE_SUCCESS':
             return{
                 ...state,
                 token: action.payload.token
             }   
        default:
            return state;
    }
};