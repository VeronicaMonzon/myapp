import React from 'react';
import footer from '../imagenes/homeIcon.png';
import { Link } from 'react-router-dom';
class Footer extends React.Component{
    constructor(props) {
        super(props);
        this.state ="";
    }
    render(){
        return(
        
          <footer className="container pt-5">
                    <Link to="/">
                        <img src={footer} className="Boton" alt="boton" />
                    </Link>
           </footer>
        
        )        
    }
           
}
export default Footer;