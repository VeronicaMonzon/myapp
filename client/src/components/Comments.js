import React, { Component } from "react";
import Submit from "../imagenes/ArrowRight.png";
import axios from "axios";


class PostComment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      comentario: ""
    };
    this.handleChange = this.handleChange.bind(this);
    this.enviarComentario = this.enviarComentario.bind(this);
  }
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  // async componentDidMount() {}
  // onChange = e => {

  //   this.setState({ comentario: e.target.value });
  // };
  enviarComentario = async e => {
    e.preventDefault();
    await axios.put(
      "http://localhost:5000/api/activity/" + this.props.idActivity,
      { comentario: this.state.comentario }
    );
    this.props.updateActivity();
  };

  render() {
    return (
      <div className="container">
        <h4>Comments</h4>
        <form>
          <div className="d-flex justify-content-start  ml-0 h-50 p-3">
            <input
              value={this.state.comentario}
              type="comment"
              name="comentario"
              className="form-control"
              id="comment1"
              onChange={this.handleChange}
            />
         
          <img src={Submit}
                 onClick={this.enviarComentario}
                 className="Boton_comment  float-right"
                 alt="boton"
          />      
           </div>
        </form>
      </div>
    );
  }
}


export default PostComment;
