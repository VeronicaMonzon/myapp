import React from "react";
import { connect } from "react-redux";
import { getActivities } from "../actions/activitiesActions";

import Slider from "./sliderActivity";
import PostComment from './Comments';
class Activities extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      expand: false,
      text: "View All"
    };
    // this.collapse=React.createRef();
  }
  changeText() {
    this.props.getActivities(this.props.idItinerary);
    this.state.expand === false
      ? this.setState({
          expand: true,
          text: "Close"
        })
      : // this.collapse.current.removeClass("collapse")
        this.setState({
          expand: false,
          text: "View All"
        });
  }

  updateActivity(){
    this.props.getActivities(this.props.idItinerary);
  }
  async componentDidMount() {
    console.log(this.props.idItinerary);
  }

  render() {
    console.log(this.props.activities);
    return (
      <div>
        <div className="collapse" id={`a${this.props.idItinerary}`}>
          <h5>Activities</h5>
          {this.props.activities.map((elm, i) => {
            return( <div key={i}>
                      <Slider details={elm.details}></Slider>
                      {elm.comments.map((comment,i)=>{
                        return(
                      <p key={i} className="text-left">Comment-{i}: {comment}
                      {/* <hr></hr> */}
                      </p>
                       
                        )
                      })}
                      <PostComment idActivity={elm._id} updateActivity={this.updateActivity}></PostComment>

                    </div>  
                      );
                      
          })}
          
        </div>
        
        <button
          data-toggle="collapse"
          type="button"
          data-target={`#a${this.props.idItinerary}`}
          aria-expanded="false"
          aria-controls={`a${this.props.idItinerary}`}
          className="w-100 bg-light"
          onClick={this.changeText.bind(this)}
        >
          {this.state.text}
        </button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    activities: state.activity.activities
  };
};

const mapDispachToProps = dispatch => {
  return {
    getActivities: idItinerary => dispatch( getActivities(idItinerary))
  };
};
export default connect(mapStateToProps, mapDispachToProps)(Activities);

