import React from "react";
import NavBar from "./NavBar";
import Footer from "./Footer";
import google from "../imagenes/Google_icon-icons.com_66793.png";
import face from "../imagenes/Facebook_icon-icons.com_66805.png";
import footer1 from "../imagenes/ArrowLeft.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { postUser } from "../actions/loginActions";

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      rebember: false,
      token: ""
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  
  handleChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  handleSubmit = async e => {
    e.preventDefault();
    
    
    let user = { email: this.state.email, password: this.state.password };
    console.log('props', this.props)

    this.props.postUser(user);
    setTimeout(()=>{
      this.props.history.push('/')
    }, 1000)

    this.setState({ email: "", password: "" });
  };
  singGoogle() {
    window.location.href = "http://localhost:5000/api/auth/google";
  }

  render() {

      return (
        <div>
        <NavBar />
        <div className="container">
          <form
            onSubmit={this.handleSubmit}
            className="container was-validated"
          >
            <h3>Login</h3>

            <div className="form-group row">
              <label
                className="col-sm-2 col-form-label text-right"
                htmlFor="email"
              >
                Email:
              </label>
              <div className="col-sm-10">
                <input
                  type="text"
                  name="email"
                  value={this.state.email}
                  onChange={this.handleChange}
                  className="form-control "
                  id="email"
                  required
                />
              </div>
            </div>

            <div className="form-group row">
              <label
                className="col-sm-2 col-form-label text-right"
                htmlFor="pwd"
              >
                Password:
              </label>
              <div className="col-sm-10">
                <input
                  type="password"
                  name="password"
                  value={this.state.password}
                  onChange={this.handleChange}
                  className="form-control"
                  id="pwd"
                  required
                />
              </div>
            </div>

            <div className="form-group row text-center">
              <input
                className="form-check-input"
                name="remember"
                type="checkbox"
                checked={this.state.rebember}
                onChange={this.handleChange}
              />
              <label className="form-check-label ">&nbsp;&nbsp;Remember me</label>
            </div>
            <button type="submit" className="btn btn-dark" >Ok</button>
            <div className="center-block">
              <button
                className="BotonGoogle"
                onClick={this.singGoogle.bind(this)}
              >
                <img style={{ height: "10vmin" }} src={google} alt="google" />
                &nbsp;&nbsp;Log in with Google
              </button>

              <button className="BotonGoogle">
                <img style={{ height: "10vmin" }} src={face} alt="face" />
                &nbsp;&nbsp;Log in with Facebook
              </button>
            </div>
            <div>
              <p style={{ fontSize: "20px" }}>
                Don't have a MYtinerary account yet? You should create one!
                It's totally free and only takes a minute.
              </p>
              <Link to="/Account">Create Account</Link>
            </div>
          </form>
          {/* <p>{JSON.stringify(this.state)} </p> */}
          <footer className="container">
            <Link className="float-left " to="/Cities">
              <img src={footer1} id="Boton1" alt="boton" />
            </Link>
            <Footer />
          </footer>
        </div>
      </div>
      )
       
      }
    }
  

const mapStateToProps = state => {
  console.log("estado de los reducers: ", state);

  return { user: state.user.user };
};


export default connect(mapStateToProps, { postUser })(Login);
