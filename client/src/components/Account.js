import React from "react";
import NavBar from "./NavBar";
import Footer from "./Footer";
import axios from "axios";
import footer1 from "../imagenes/ArrowLeft.png";
import { Link } from "react-router-dom";


class Account extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      pass: "",
      picUrl: "",
      user: "",
      first: "",
      last: "",
      iAgree: false,
      country: "choose"
    };

    this.handleChange = this.handleChange.bind(this);
    this.saveUser = this.saveUser.bind(this);
  }

  handleChange(event) {
    // const target = event.target;
    // const value = target.type === 'text' ? target.onChange : target.value;
    // const name = target.name;

    // this.setState({
    //   [name]: value
    // });

    this.setState({ [event.target.name]: event.target.value });
  }

  saveUser = async e => {
    e.preventDefault();
    
    var { email, pass, picUrl, user,first,last,country } = this.state;

    await axios
      .post("http://localhost:5000/api/users", { email, pass, picUrl, user,first,last,country })
      .then(res => {
      
        console.log(res);
        console.log(res.data);
      }).catch((err)=>console.log(err))
    this.setState({ email: "", pass: "", picUrl: "", user: "",first:"",last:"",country:""});
  };

  render() {
    return (
      <div>
        <NavBar />
        {/* <Header/> */}

        <form onSubmit={this.handleSubmit} className="container was-validated" >
          <h3>Create Account</h3>
          <div id="circle" className="">
          {/* <input type="file" name="myFile"/> */}
            <a href="#">Add photo</a>
          </div>
          <br></br>
          <div className="form-group row">
            <label
              className="col-sm-2 col-form-label text-right"
              htmlFor="user"
            >
              UserName:
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                name="user"
                value={this.state.user}
                onChange={this.handleChange}
                className="form-control "
                id="user"
                required
              />
            </div>
            <div class="valid-feedback"></div>
            <div class="invalid-feedback">Please fill out this field.</div>
          </div>
          <div className="form-group row">
            <label
              className="col-sm-2 col-form-label text-right"
              htmlFor="email"
            >
              Email:
            </label>
            <div className="col-sm-10">
              <input
                type="email"
                name="email"
                value={this.state.email}
                onChange={this.handleChange}
                className="form-control"
                id="email"
                required
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label text-right" htmlFor="pwd">
              Password:
            </label>
            <div className="col-sm-10">
              <input
                type="password"
                name="pass"
                value={this.state.pass}
                onChange={this.handleChange}
                className="form-control"
                id="pwd"
                required
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label text-right" htmlFor="url">
              URL-Picture:
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                name="picUrl"
                value={this.state.picUrl}
                onChange={this.handleChange}
                className="form-control"
                id="url"
                
              />
            </div>
          </div>
          <div className="form-group row">
            <label
              className="col-sm-2 col-form-label text-right"
              htmlFor="first"
            >
            FirstName:
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                name="first"
                value={this.state.first}
                onChange={this.handleChange}
                className="form-control"
                id="first"
              />
            </div>
          </div>
          <div className="form-group row">
            <label
              className="col-sm-2 col-form-label text-right"
              htmlFor="last"
            >
              LastName:
            </label>
            <div className="col-sm-10">
              <input
                type="text"
                name="last"
                value={this.state.last}
                onChange={this.handleChange}
                className="form-control"
                id="last"
              />
            </div>
          </div>
          <div className="form-group row">
            <label className="col-sm-2 col-form-label text-right">
              Country:&nbsp;
            </label>
            <div className="col-sm-10 text-left">
              <select
                name="country"
                value={this.state.country}
                onChange={this.handleChange}
                required
              >
                <option value="choose">Choose...</option>
                <option value="england">England</option>
                <option value="france">France</option>
                <option value="germany">Germany</option>
                <option value="holland">Holland</option>
                <option value="ireland">Ireland</option>
                <option value="spain">Spain</option>
                <option value="unitedState">United State</option>
              </select>
            </div>
          </div>
          <div className="form-group row text-center">
            <input
              className="form-check-input"
              name="iAgree"
              type="checkbox"
              checked={this.state.iAgree}
              onChange={this.handleChange}
              required
            />
            <label className="form-check-label ">
              &nbsp;I agree to Mytinerary's <a href="#">Terms & Conditions</a>{" "}
            </label>
          </div>
          <input
            onClick={this.saveUser}
            type="submit"
            className="btn btn-dark"
            value="Submit"
          />
        </form>
        {/* <p>{JSON.stringify(this.state)} </p> */}
        <footer className="container">
          <Link className="float-left " to="/Login">
            <img src={footer1} id="Boton1" alt="boton" />
          </Link>
          <Footer />
        </footer>
      </div>
    );
  }
}
export default Account;
