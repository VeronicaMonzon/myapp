import * as React from "react";
import Whirligig from "react-whirligig";

const Slider = props => {
  let whirligig;
  //   const next = () => whirligig.next();
  //   const prev = () => whirligig.prev();

  return (
    <div>
      <Whirligig
        visibleSlides={3}
        gutter="1em"
        ref={_whirligigInstance => {
          whirligig = _whirligigInstance;
        }}
      >
        {props.details.map((i, j) => {
          return (
            <div key={j}>
              <img
                
                src={i.activityPic}
                alt={i.title}
                style={{ width: "150px", height: "150px" }}
              />
              <p>{i.title}</p>
            </div>
          );
        })}
      </Whirligig>
    </div>
  );
};
export default Slider;
