import React, { useState } from 'react';
import './Carousel.css'
import {
	Carousel,
	CarouselItem,
	CarouselControl,
	CarouselIndicators,
	Container,
	Row,
	Col
} from 'reactstrap';
/* images */
import amsterdam from './imgCarousel/Amsterdam.jpg';
import barcelona from './imgCarousel/Barcelona1.jpg';
import dubai from './imgCarousel/Dubai.jpg';
import argentina from './imgCarousel/Argentina.jpg';
import Paris from './imgCarousel/Paris.jpg';
import newyork from './imgCarousel/New_york.jpg';
import Roma from './imgCarousel/Roma.jpg';
import grecia from './imgCarousel/Grecia.jpg';
import egipto from './imgCarousel/Egipto.jpg';
import venecia from './imgCarousel/venecia.jpg'
import japon from './imgCarousel/Japon.jpg'
import inglaterra from './imgCarousel/Inglaterra.jpg'
const items = [
	[
		{
			src: amsterdam,
			alt: 'Amsterdam'
		},
		{
			src: barcelona,
			alt: 'Barcelona'
		},
		{
			src: dubai,
			alt: 'Dubai'
		},
		{
			src: argentina,
			alt: 'Argentina'
		}
	],
	[
		{
			src: Paris,
			alt: 'Paris'
		},
		{
			src: newyork,
			alt: 'New york'
		},
		{
			src: Roma,
			alt: 'Roma'
		},
		{
			src: grecia,
			alt: 'Grecia'
		}
	],
	[
		{
			src: egipto,
			alt: 'Egipto'
		},
		{
			src: venecia,
			alt: 'Venecia'
		},
		{
			src: inglaterra,
			alt: 'Inglaterra'
		},
		{
			src: japon,
			alt: 'Japon'
		}
	]
];

const Example = props => {
	const [activeIndex, setActiveIndex] = useState(0);
	const [animating, setAnimating] = useState(false);

	const next = () => {
		if (animating) return;
		const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
		setActiveIndex(nextIndex);
	};

	const previous = () => {
		if (animating) return;
		const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
		setActiveIndex(nextIndex);
	};

	const goToIndex = newIndex => {
		if (animating) return;
		setActiveIndex(newIndex);
	};

	const slides = items.map((g, i) => {
		return (
			<CarouselItem
				onExiting={() => setAnimating(true)}
				onExited={() => setAnimating(false)}
				key={i}
			>
				<Container>
					<Row>
						{g.map(image => {
							return (
								<Col
									xs={{ size: 6 }}
									key={image.alt}
																		
								>
									<div className="figure">
										<p>
											<img className="scaled" src={image.src} alt={image.alt} style={{ height: '12rem'}} />
										    {image.alt} 
										</p>
                                    </div>
								</Col>
							);
						})}
					</Row>
				</Container>
			</CarouselItem>
		);
	});

	return (
		<Carousel activeIndex={activeIndex} next={next} previous={previous}>
			<CarouselIndicators
				items={items.map(group => ({ src: group[0].src }))}
				activeIndex={activeIndex}
				onClickHandler={goToIndex}
			/>
			{slides}
			<CarouselControl
				direction='prev'
				directionText='Previous'
				onClickHandler={previous}
			/>
			<CarouselControl
				direction='next'
				directionText='Next'
				onClickHandler={next}
			/>
		</Carousel>
	);
};

export default Example;