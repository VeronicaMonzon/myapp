import React from 'react';
import {Link} from 'react-router-dom'
import logo from '../imagenes/MYtineraryLogo.png';
import footer from '../imagenes/homeIcon.png';
import flecha from '../imagenes/circled-right-2.png'

export const Header = ({logo})=>{
    return (
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <p>Find your perfect trip, designed by insiders who know and love their cities</p>
        </header>
    )
}

function Landing() {

    return (<div>
        
        <Header logo={logo}/>
        <div className="container text-center">
            <h1>Start browsing</h1>
            <Link to="/Cities"><img src={flecha} className="Boton" alt="flecha" /></Link>
            <h4>Want to build your own Mytinerary?</h4>
        </div>
        <div className="d-flex justify-content-around">            
             <Link to="/Login">Login</Link>
             <Link to="/Account">Create Account</Link> 
               
        </div>

        <footer className="container">
            <img src={footer} className="Boton" alt="boton" />
        </footer>
    </div>
    );
}
export default Landing;