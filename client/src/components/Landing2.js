import React from 'react';
import { Link } from 'react-router-dom'
import logo from '../imagenes/MYtineraryLogo.png';
/* REACTSTRAP */
import { Container, Row, Col } from 'reactstrap';
/* COMPONENTS */
import Header from './Header'
import NavBar from './NavBar'
// import Carousel from './Carousel';
import arrow from '../imagenes/circled-right-2.png'
import Example from './Carousel'
function Landing2() {

    return (<div>
        <NavBar/>
        <Header logo={logo} />
        <Container>
				<Row>
					<Col xs={{ size: 8, offset: 2 }} className='mt-3'>
						<p className='text-center'>
							Find your perfect trip, designed by insiders who know
							and love their cities.
						</p>
					</Col>
					<Col xs={{ size: 6, offset: 3 }}>
						<Link
							to='/cities'
							className='d-flex justify-content-center '
						>
							<img
								src={arrow}
								alt='arrow'
								className='img-fluid w-50'
							/>
						</Link>
					</Col>
				</Row>
				<Row>
					<Col>
						<p className='display-5'>Popular Mytineraries</p>
						<Example />
					</Col>
				</Row>
			</Container>
        
       
    </div>
    );
}
export default Landing2;