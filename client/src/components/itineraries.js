import React from "react";
import { connect } from "react-redux";
import getItineraries from "../actions/itinerariesActions";
import { getCity } from "../actions/citiesActions";
import Footer from "./Footer";
import footer1 from "../imagenes/ArrowLeft.png";
import { Link } from "react-router-dom";
import { IoIosHeart, IoMdHeartEmpty} from "react-icons/io";
import NavBar from '../components/NavBar'
// import { getActivities } from '../actions/activitiesActions';
import Activities from "./Activities";

class Itineraries extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      liked:false
    };
  
  }
 
  async componentDidMount() {
    await this.props.getItineraries(this.props.match.params.idCity);
    await this.props.getCity(this.props.match.params.idCity);
    // await this.props.getActivities(this.props.match.params.idItinerary);
  }
likes(){
    if (this.state.liked){
        this.setState({liked:false })        
       }
     else{
        this.setState({liked:true })
    }
}
  render() {
    // console.log(this.props.city.url);
    const liked=this.state.liked
    return (
      <div>
          <NavBar/>
        <div className=" card text-white text-center d-flex ">
          <div className="card-img-overlay">
            <p
              className="card-text mt-5"
              style={{
                textShadow: "2px 2px  #000000",
                fontSize: "30px",
                fontWeight: "bold",
                color: "white"
              }}
            >
              {this.props.city.city} {this.props.city.country}
            </p>
          </div>

          <img
            style={{ height: "150px" }}
            src={this.props.city.url}
            alt={this.props.city.city}
          />
        </div>

        <div className="p-3">
          <div
            className="p-3 text-left"
            style={{
              fontSize: "30px",
              fontWeight: "bold"
            }}
          >
            <h4>Available MYtineraries:</h4>
          </div>

          {this.props.itineraries.map((elm, i) => {
            return (
              <div key={i}>
                <div className="media border">
                  <div className="media-left pr-3 pt-3">
                    <img
                      style={{ width: "90px", borderRadius: "50%" }}
                      src={elm.profilePic}
                      alt={elm.title}
                    />
                    <p style={{ fontSize: "12px" }}> {elm.user}</p>
                  </div>

                  <div
                    className="media-body width=60%"
                    style={{
                      fontSize: "18px",
                      color: "black"
                    }}
                  >
                    <h4
                      className="media-heading"
                      style={{
                        textShadow: "2px 2px 5px #000000",
                        fontSize: "24px",
                        fontWeight: "bold"
                      }}
                    >
                      {" "}
                      {elm.title}
                    </h4>
                    <div className="d-flex justify-content-between pr-3 pt-3 ">
                      <div> Likes: {elm.rating}</div>
                      <div> {elm.duration} Hours</div>
                      <div> Price:${elm.price} </div>
                    </div>
                    <div className="d-flex justify-content-start pt-4 ">                                      
                   <div >
                  
                    {liked ?    
                     ( <IoIosHeart
                        style={{ color: "rgb(204, 3, 3)"}}
                        onClick={this.likes.bind(this)}
                      />):(

                    <IoMdHeartEmpty
                      onClick={this.likes.bind(this)}
                      /> )}
                      </div>
                      &nbsp;&nbsp;&nbsp;
                      {elm.hashtag.map((tag, index) => (
                        <small key={index}> #{tag} </small>
                      ))}
                  </div>
                  </div>
                </div>

                <Activities idItinerary={elm._id} />
              </div>
            );
          })}

          <Link to="/cities"> Chose another city</Link>
        </div>
        <footer className="container">
          <Link className="float-left" to="/cities">
            <img src={footer1} className="Boton" alt="boton" />
          </Link>
          <Footer />
        </footer>
      </div>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    //reducer
    itineraries: state.itinerary.itineraries,
    city: state.city.city
    // activities: state.activity.activities
  };
};

const mapDispachToProps = dispatch => {
  return {
    getItineraries: id => dispatch(getItineraries(id)),
    getCity: idCity => dispatch(getCity(idCity))
    // getActivities: (idItinerary) => dispatch(getActivities(idItinerary))
  };
};
export default connect(mapStateToProps, mapDispachToProps)(Itineraries);
