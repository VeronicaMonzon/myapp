import React from 'react';
// import Header from './Header';
import { connect } from 'react-redux';
import getAllCities from '../actions/citiesActions';
// import footer from '../imagenes/homeIcon.png';
import { Link } from 'react-router-dom'
import NavBar from './NavBar';
import Footer from './Footer';
// import FilterResults from 'react-filter-search';

// const url = 'http://localhost:5000/cities/all'

class Cities extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            data: []
        }

    }

    async componentDidMount() {

        await this.props.getAllCities();
        console.log(this.props.cities)
        this.setState({ data: this.props.cities })

    }
    filterCities = (e) => {
        let filteredCities = this.props.cities
        filteredCities = filteredCities.filter((name) => {
            let cityName = name.city.toLowerCase();

            let mipalabra = e.target.value.toLowerCase();

            let arraycity = cityName.split("");
            let arraypalab = mipalabra.split("");
            let flag = false;
            arraypalab.forEach((element, i) => {
                if (element !== arraycity[i]) {
                    flag = true;
                }
            });
            if (!flag) {
                return name;
            } else {
                return null;
            }
        })
        this.setState({
            data: filteredCities
        })
        return;
    }


    render() {

        return (
            <div>
                <NavBar />
                {/* <Header /> */}
                <div className="container p-4 text-white bg-dark ">
                    <h4>Filter our current cities</h4>
                </div>
                <div className="container w-92% mt-5">
                    <input id="desplazar" className="container" placeholder="Search"
                        onChange={this.filterCities}
                    ></input>
                </div>
                <div className="p-3" >

                    {
                        this.state.data.length !== 0 ? (
                            this.state.data.map((elm, i) => {
                                return <div className=" card text-white text-center d-flex "

                                    key={i}>
                                    <div className="card-img-overlay">
                                        <Link to={`/itineraries/${elm._id}`}>
                                            <p className="card-text mt-5" style={{
                                                textShadow: "2px 2px  #000000", fontSize: "30px",
                                                fontWeight: "bold", color: "white"
                                            }} >{elm.city} {elm.country}</p>
                                        </Link>
                                    </div>


                                    <img style={{ height: "150px" }} src={elm.url} alt={elm.city} />


                                </div>
                            })
                        ) : (
                                <p className="card bg-dark text-white">No results found</p>
                            )
                    }
                </div>
                {/* <footer className="container">
                    <Link to="/">
                        <img src={footer} className="Boton" alt="boton" />
                    </Link>
                </footer> */}
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    console.log('estado de los reducers: ', state);
    
    return { cities: state.city.cities }
}

const mapDispachToProps = (dispatch) => {
    return { getAllCities: () => dispatch(getAllCities()) }
}
export default connect(mapStateToProps, mapDispachToProps)(Cities);