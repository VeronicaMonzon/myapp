import React from "react";
import avatar from "../imagenes/avatar.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { postUser,logOut} from "../actions/loginActions";


class NavBar extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
              
      };
  
    }

  render() {
    const isLoged=this.props.user.isAuth 
  return (
   
    <nav className="navbar navbar-expand-md p-1 navbar-light">
    <div>
   
      { 
      isLoged ? (
        <div className="dropdown">
          <img
            className=" dropdown-toggle"
            data-toggle="dropdown"
            style={{ height: "12vmin", borderRadius: "100px" }}
            src={this.props.user.user.picUrl}
            alt="avatar"
          />

          <div className="dropdown-menu ">
            <Link to="/Login" className="dropdown-item"  onClick={this.props.logOut}>
              Log Out
            </Link>
          </div>
        </div>
      ) : (
        <div className="dropdown">
          <img
            className=" dropdown-toggle"
            data-toggle="dropdown"
            style={{ height: "11vmin", borderRadius: "100px" }}
            src={avatar}
            alt="avatar"
          />
          <div className="dropdown-menu ">
            <Link to="/Login" className="dropdown-item">
              Log in
            </Link>
            <Link to="/Account" className="dropdown-item">
              Create Account
            </Link>
          </div>
        </div>
      )}
    </div>

    <button
      className="navbar-toggler"
      type="button"
      data-toggle="collapse"
      data-target="#collapsibleNavbar"
    >
      <span className="navbar-toggler-icon navbar-light"></span>
    </button>
    <div
      className="collapse navbar-collapse navbar-light"
      id="collapsibleNavbar"
    >
      <ul className="navbar-nav navbar-light">
        <li className="nav-item ">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/" className="nav-link">
           Home
          </Link>
        </li>
        <li className="nav-item">
          <Link to="/" className="nav-link">
            Home
          </Link>
        </li>
      </ul>
    </div>
  </nav>
  )
   
  }}

const mapStateToProps = state => ({
  user: state.user,
 logOut:state.logOut
});

export default connect(mapStateToProps, { postUser,logOut })(NavBar);
