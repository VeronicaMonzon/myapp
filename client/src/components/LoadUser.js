import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { googleUser, googleSuccess } from "../actions/loginActions";

class loadUser extends Component {
  componentWillMount() {
    if (this.props.match.params.token) {
      const token = this.props.match.params.token;
      window.localStorage.setItem("token", token);
      this.props.googleSuccess(token);
      this.props.googleUser(this.props);
    }
  }
  render() {
    return <Redirect to="/" />;
  }
}

const mapStateToProps = loginReducer => {
  return loginReducer;
};
export default connect(mapStateToProps,{ googleUser, googleSuccess })(loadUser);
