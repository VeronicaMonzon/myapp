var express = require('express');
var app     = express();
var port    =   process.env.PORT || 5000;
var cors = require('cors');
const bodyParser= require('body-parser');
require('./connect')
const cityApi=require('./src/routes/cityApi')
const itineraryApi=require('./src/routes/itineraryApi')
const activityApi = require('./src/routes/activityApi')
const userApi=require('./src/routes/userApi');
const passport=require('./src/passport/passport')
const loginApi= require('./src/routes/loginApi')
const localApi = require('./src/routes/local');
const favItinerary = require('./src/routes/favItinerariesApi')
//passport middleware
app.use(passport.initialize());
app.use(cors())
app.use(bodyParser())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended:true}))

app.use('/api', cityApi);
app.use('/api', itineraryApi);
app.use('/api', activityApi);
app.use('/api',userApi);
app.use('/api',loginApi);
app.use('/api',localApi);
app.use('/api',favItinerary);
// we'll create our routes here

// START THE SERVER
// ==============================================
app.listen(port);
console.log('Magic happens on port ' + port);
