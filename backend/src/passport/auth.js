const express = require("express");
const passport = require("./passport");
var userModel = require("../models/user");
var GoogleStrategy = require("passport-google-oauth20").Strategy;

passport.use(
  new GoogleStrategy(
    {
      clientID:
        "292240438129-hg63ahh4fjt50hnqbid29t69sg2uba2u.apps.googleusercontent.com",
      clientSecret: "DKUI9GMCB7sDu4Oj3rOX-XGv",
      callbackURL: "http://localhost:5000/api/auth/google/callback"
    },
    async function(accessToken, refreshToken, profile, cb) {
      // console.log("linea-12", profile);

      const email = profile.emails[0].value;
      const UserRequired = await userModel.findOne({ email });
      if (!UserRequired) {
        const newUser = new userModel({
          email: profile.emails[0].value,
          picUrl: profile.photos[0].value,
          user: profile.emails[0].value,
          first: profile.name.givenName,
          last: profile.name.familyName
          // country:req.body.country
        });
        const userCreated = await newUser.save();
        cb(null, userCreated);
      } else {
        cb(null, UserRequired);
      }
    }
  )
);
