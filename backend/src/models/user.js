var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var userSchema=new Schema({
    email:{
        type: String
    },
    password:{
        type: String
    },
    picUrl:{
        type: String
    },
    user:{
        type: String
    },
    first:{
      type:String
    },
    last:{
        type:String
    },
    country:{
        type:String
    },
    favItineraries:{
        type:Array
    }
},
{
    collection:'users'
}     
    
)
module.exports=User=mongoose.model("User",userSchema)