var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var itinerarySchema=new Schema({
    title:{
        type: String
    },
    profilePic:{
        type: String
    },
    rating:{
        type: Number
    },
    duration:{
        type: Number
    },
    price:{
        type: Number
    },
    hashtag:{
        type: Array
    },
    id_city:{
        type: mongoose.Types.ObjectId
    },
    user:{
        type: String
    }

},

{
    collection:'itineraries'
}     
    
)
module.exports=Itinerary=mongoose.model("Itinerary",itinerarySchema)