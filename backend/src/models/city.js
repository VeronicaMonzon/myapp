var mongoose=require('mongoose');
var Schema=mongoose.Schema;
var citySchema=new Schema(
    {
        city: String,
        country: String,
        img: String 
    },
    {
        collection:'cities'
    }
)
module.exports=mongoose.model("cities",citySchema)