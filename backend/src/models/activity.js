var mongoose=require('mongoose');
var Schema=mongoose.Schema;



var activitySchema=new Schema({
    details:[{
        title:{
            type: Array
        },
        activityPic:{
            type:Array
        }
     
    }],
    comments:{
        type: Array
    },
    
    id_itinerary:{
        type: mongoose.Types.ObjectId
    },
   
},

{
    collection:'activities'
}     
    
)
module.exports=Activity=mongoose.model("Activity",activitySchema)