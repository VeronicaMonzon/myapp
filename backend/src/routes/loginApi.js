const express = require("express");
const router = express.Router();
var userModel = require("../models/user");
const bcrypt = require("bcryptjs");
const Key = require("../config/vars");
const jwt = require("jsonwebtoken");
const passport = require("../passport/passport");

const { check, validationResult } = require("express-validator");

router.post(
  "/login",
  [check("email").isEmail(), check("password").isLength({ min: 4 })],
  async (req, res) => {
    // console.log("linea 14 login api" + req.body.email);
    const errors = validationResult(req.body);
    console.log(errors);
    const { email, password } = req.body;
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() });
    }

    const User = await userModel.findOne({ email });
    // console.log("linea 23" + User);

    if (!User) {
      return res.status(400).json({ msg: "Username does not exist" });
    } else {
      const isCorrect = await bcrypt.compare(password, User.password);
      delete User.password
      if (!isCorrect) {
        return res.status(400).json({ msg: "Invalid password" });
      }
    }

    const options = { expiresIn: 648000 };
    jwt.sign(
      { id: User._id, picUrl: User.picUrl, user: User.user },
      Key.secretOrKey,
      options,
      (err, token) => {
        if (err) {
          res.json({
            success: false,
            token: "There was an error"
          });
        } else {
          console.log(token);
          res.json({
            success: true,
            token: token,
            // user: User
          });
        }
      }
    );
  }
);

module.exports = router;
