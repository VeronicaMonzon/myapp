const express=require("express")
const router=express.Router()
var itineraryModel=require('../models/Itinerary');


router.get("/:id_city", async (req, res) => {
    const id_city = req.params.id_city;
    const Itinerary = await itineraryModel.find({ id_city });
    res.json(Itinerary);
  });
  
  
module.exports=router