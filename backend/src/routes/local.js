const express = require("express");
const router = express.Router();
const jwt = require('jsonwebtoken')
const passport = require("../passport/passport");
require("../passport/auth")
var userModel = require("../models/user");
const { config } = require('../config')

router.get("/", function(req, res) {
  res.send("OK");
});

router.get(
  "/login",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    userModel
      .findOne({ _id: req.user.id })
      .then(user => {
        res.json(user);
      })
      .catch(err => res.status(404).json({ error: "User does not exist!" }));
  }
);
router.get(
  "/auth/google",
  passport.authenticate("google", { scope: ["profile","email"] })
);
const token="";
router.get(
  "/auth/google/callback",
  passport.authenticate("google", { failureRedirect: "http://localhost:3000/Login" ,session:false}),
  async function(req, res) {

    console.log('User registrado',req.user)

    const payload = {
      id: req.user._id,
      email: req.user.email,
      user: req.user.user,
      picUrl:req.user.picUrl
    }

    token = await jwt.sign(payload, config.jwtSecret, { expiresIn: '720m'})
 console.log(token)
    // Successful authentication, redirect home.
    // res.json(user);
    
    res.redirect(`http://localhost:3000/loaduser/${token}`)//.json({
    //   token,
    //   user:{
    //     user,
    //     picUrl

    //   }
    // })
  }
);

module.exports = router;
