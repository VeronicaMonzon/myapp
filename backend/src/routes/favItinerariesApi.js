const express = require("express");
const router = express.Router();
const passport = require("../passport/passport");
const ItineraryModel = require("../models/Itinerary");
const UserModel = require("../models/user");

router.post(
  "/favItineraries/:IdItinerary",
  passport.authenticate("jwt", {session: false}),
  async function(req, res) {
    const { IdItinerary } = req.params;
    console.log(IdItinerary);
    console.log("User!!!", req.user);

    const ItineraryRequired = await ItineraryModel.findById(IdItinerary);
    const UserRequired = await UserModel.findById(req.user._id);
    console.log({ ItineraryRequired, UserRequired });

    res.send("OK")
  }
);
router.delete("/favItineraries/:IdItinerary", function(req, res) {});
module.exports = router;
