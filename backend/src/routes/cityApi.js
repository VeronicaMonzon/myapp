const express=require("express")
const router=express.Router()
var cityModel=require('../models/city');


router.get('/cities/all', function (req,res) {
    cityModel.find()
    .catch(function(err) {
        console.error(err)
    })

    .then(function (datos) {
        return res.send(datos)
    })
})
router.get('/city/:idCity', function (req,res) {
    cityModel.findOne({_id:req.params.idCity})
    .catch(function(err) {
        console.error(err)
    })

    .then(function (datos) {
        return res.send(datos)
    })
})
module.exports=router