const express=require("express")
const router=express.Router()
var activityModel=require('../models/activity');

router.get('/activities/:idItinerary', function (req,res) {
    const {idItinerary} = req.params
    
    activityModel.find({id_itinerary:idItinerary})
    .catch(function(err) {
        console.error(err)
    })

    .then(function (datos) {
        console.log(datos)
        return res.status(200).json(datos)
    })
})
router.put("/activity/:id", async (req, res) => {

    
    var Activity= await activityModel.findById(req.params.id);
    const comment = req.body.comentario;
    Activity.comments.push(comment)
    console.log(Activity);
    const comments=Activity.comments
    
    const resp=await activityModel.update({_id:req.params.id}, Activity);
    res.status(201).json(resp);
  });

module.exports=router