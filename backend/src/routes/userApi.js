const express=require("express")
const router=express.Router()
var userModel=require('../models/user');
const bcrypt=require('bcryptjs');
const { check, validationResult } = require('express-validator');
router.get('/users/all', function (req,res) {
    userModel.find()
    .catch(function(err) {
        console.error(err)
    })

    .then(function (datos) {
        return res.send(datos)
    })
})
router.get('/users/:idUser', function (req,res) {
    userModel.findOne({_id:req.params.idUser})
    .catch(function(err) {
        console.error(err)
    })

    .then(function (datos) {
        return res.send(datos)
    })
})

router.post('/users',[check('email').isEmail(),check('password').isLength({ min: 4 }),check('user').isLength({min:3})], (req, res) => {
    console.log(req.body.email)
    const errors = validationResult(req.body);
    console.log(errors)
    if (!errors.isEmpty()) {
      return res.status(422).json({ errors: errors.array() })
    }
    var salt = bcrypt.genSaltSync(10);
    // var hash = bcrypt.hashSync(req.body.password, salt);
    var hash = bcrypt.hashSync(req.body.pass, salt);
    
    const newUser = new userModel({
        email: req.body.email,
        password: hash,
        picUrl: req.body.picUrl,
        user:req.body.user,
        first:req.body.first,
        last:req.body.last,
        country:req.body.country
    })
    const email=newUser.email;
    userModel.findOne({email:email}).then((findUser)=>{
    
        if (findUser) {
            return res.status(500).send("User already exists")
           }
           else{
              newUser.save()
              .then(user => {
              res.send(user)
              })
              .catch(err => {
              res.status(500).send("Server error")}) 
           }
          

    })
    

   
});

module.exports=router